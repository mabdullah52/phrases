class HomeController < ApplicationController

  PHRASES = [
    'sample phrase number 1',
    'sample phrase number 2',
    'sample phrase number 3',
    'sample phrase number 4',
    'sample phrase number 5',
    'sample phrase number 6',
    'sample phrase number 7',
    'sample phrase number 8',
    'sample phrase number 9',
    'sample phrase number 10',
  ]

  def index
    initialize_phrases
  end

  def get_phrase
    if session['pharases_count'] < PHRASES.length
      session['phrases'] << (PHRASES - session['phrases']).sample
      session['pharases_count'] += 1
    end

    respond_to do |format|
      format.js
    end
  end

  def initialize_phrases
    session['pharases_count'] ||= 0
    session['phrases'] ||= []
  end

  def reset_phrases
    session['pharases_count'] = 0
    session['phrases'] = []
    render "get_phrase"
  end

end
